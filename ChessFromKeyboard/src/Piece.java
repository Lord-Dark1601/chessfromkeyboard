//
public class Piece {

	private TypeOfPiece type;
	private Color color;

	public Piece(TypeOfPiece type, Color color) {
		this.type = type;
		this.color = color;
	}

	public Piece(String type, String color) {
		TypeOfPiece t = null;
		Color c = null;
		type = type.toUpperCase();
		if (color.equalsIgnoreCase("B")) {
			switch (type) {
			case "P":
				t = TypeOfPiece.PEON;
				c = Color.BLANCO;
				break;
			case "C":
				t = TypeOfPiece.CABALLO;
				c = Color.BLANCO;
				break;
			case "A":
				t = TypeOfPiece.ALFIL;
				c = Color.BLANCO;
				break;
			case "T":
				t = TypeOfPiece.TORRE;
				c = Color.BLANCO;
				break;
			case "D":
				t = TypeOfPiece.DAMA;
				c = Color.BLANCO;
				break;
			case "R":
				t = TypeOfPiece.REY;
				c = Color.BLANCO;
				break;
			}
		} else {
			switch (type) {
			case "P":
				t = TypeOfPiece.PEON;
				c = Color.NEGRO;
				break;
			case "C":
				t = TypeOfPiece.CABALLO;
				c = Color.NEGRO;
				break;
			case "A":
				t = TypeOfPiece.ALFIL;
				c = Color.NEGRO;
				break;
			case "T":
				t = TypeOfPiece.TORRE;
				c = Color.NEGRO;
				break;
			case "D":
				t = TypeOfPiece.DAMA;
				c = Color.NEGRO;
				break;
			case "R":
				t = TypeOfPiece.REY;
				c = Color.NEGRO;
				break;
			}
		}
		this.type = t;
		this.color = c;

	}

	public TypeOfPiece getType() {
		return type;
	}

	public Color getColor() {
		return color;
	}

	@Override
	public String toString() {
		String s;
		if (color == Color.NEGRO) {
			s = "BLACK";
		} else {
			s = "WHITE";
		}
		s += "-";
		switch (type) {
		case PEON:
			s = s + "PAWN";
			break;
		case TORRE:
			s = s + "TOWER";
			break;
		case CABALLO:
			s = s + "KNIGHT";
			break;
		case ALFIL:
			s = s + "BISHOP";
			break;
		case DAMA:
			s = s + "QUEEN";
			break;
		case REY:
			s = s + "KING";
			break;
		}
		return s;
	}

	public String getPiece() {
		String s = "";
		if (color == Color.BLANCO) {
			switch (type) {
			case PEON:
				s = "\u2659";
				break;
			case TORRE:
				s = "\u2656";
				break;
			case CABALLO:
				s = "\u2658";
				break;
			case ALFIL:
				s = "\u2657";
				break;
			case DAMA:
				s = "\u2655";
				break;
			case REY:
				s = "\u2654";
				break;
			}
		} else {
			switch (type) {
			case PEON:
				s = "\u265F";
				break;
			case TORRE:
				s = "\u265C";
				break;
			case CABALLO:
				s = "\u265E";
				break;
			case ALFIL:
				s = "\u265D";
				break;
			case DAMA:
				s = "\u265B";
				break;
			case REY:
				s = "\u265A";
				break;
			}
		}
		return s;
	}

}
