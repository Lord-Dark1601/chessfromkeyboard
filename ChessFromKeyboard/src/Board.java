
public class Board {

	private Piece[][] board;
	public static final int SIZE = 8;

	public Board() {
		board = new Piece[SIZE][SIZE];

		board[0][0] = new Piece(TypeOfPiece.TORRE, Color.BLANCO);
		board[0][1] = new Piece(TypeOfPiece.CABALLO, Color.BLANCO);
		board[0][2] = new Piece(TypeOfPiece.ALFIL, Color.BLANCO);
		board[0][3] = new Piece(TypeOfPiece.DAMA, Color.BLANCO);
		board[0][4] = new Piece(TypeOfPiece.REY, Color.BLANCO);
		board[0][5] = new Piece(TypeOfPiece.ALFIL, Color.BLANCO);
		board[0][6] = new Piece(TypeOfPiece.CABALLO, Color.BLANCO);
		board[0][7] = new Piece(TypeOfPiece.TORRE, Color.BLANCO);

		board[7][0] = new Piece(TypeOfPiece.TORRE, Color.NEGRO);
		board[7][1] = new Piece(TypeOfPiece.CABALLO, Color.NEGRO);
		board[7][2] = new Piece(TypeOfPiece.ALFIL, Color.NEGRO);
		board[7][3] = new Piece(TypeOfPiece.DAMA, Color.NEGRO);
		board[7][4] = new Piece(TypeOfPiece.REY, Color.NEGRO);
		board[7][5] = new Piece(TypeOfPiece.ALFIL, Color.NEGRO);
		board[7][6] = new Piece(TypeOfPiece.CABALLO, Color.NEGRO);
		board[7][7] = new Piece(TypeOfPiece.TORRE, Color.NEGRO);

		for (int col = 0; col < board.length; col++) {
			board[1][col] = new Piece(TypeOfPiece.PEON, Color.BLANCO);
			board[6][col] = new Piece(TypeOfPiece.PEON, Color.NEGRO);
		}

		for (int row = 2; row < board.length - 2; row++) {
			for (int col = 0; col < board[0].length; col++) {
				board[row][col] = null;
			}
		}
	}

	@Override
	public String toString() {
		String s = "";
		Piece p;
		for (int row = board.length - 1; row >= 0; row--) {
			s += (row + 1);
			for (int col = 0; col < board[0].length; col++) {
				p = board[row][col];
				if (p != null) {
					s += " " + p.getPiece();
				} else {
					s += " ·";
				}
			}
			s += "\n";
		}
		s += " ";
		for (char c = 'A'; c <= 'H'; c++) {
			s += " " + c;
		}
		s += "\n";
		return s;
	}

	public Piece getPieceAt(char col, int row) {
		int realRow = row - 1;
		int realCol = col - 'A';
		return board[realRow][realCol];
	}
}
